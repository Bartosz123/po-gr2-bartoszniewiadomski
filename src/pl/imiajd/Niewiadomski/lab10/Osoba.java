package pl.imiajd.Niewiadomski.lab10;
import java.time.LocalDate;

public class Osoba implements Cloneable, Comparable<Osoba> {
    private String nazwisko;
    private LocalDate dataUrodzenia;

    public String getNazwisko() {
        return nazwisko;
    }
    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }
    public Osoba(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }
    @Override
    public String toString() {
        return this.getClass() + " [Nazwisko: " + nazwisko + " Data urodzenia: " + dataUrodzenia + "]";
    }
    @Override
    public boolean equals(Object otherObject) {
        Osoba temp = (Osoba) otherObject;
        if(this.nazwisko.equals(temp.nazwisko) && this.dataUrodzenia.equals(temp.dataUrodzenia)) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(Osoba o) {
        if(this.nazwisko.compareTo(o.nazwisko) == 0){
            return this.dataUrodzenia.compareTo(o.dataUrodzenia);
        }
        return this.nazwisko.compareTo(o.nazwisko);
    }
}
