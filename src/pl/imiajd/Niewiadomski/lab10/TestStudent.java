package pl.imiajd.Niewiadomski.lab10;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main(String[] args) {
        ArrayList<Student> grupa = new ArrayList<>();

        grupa.add(new Student("Kowalski", LocalDate.of(1398, 12, 24), 4.0));
        grupa.add(new Student("Stonoga", LocalDate.of(1546, 10, 5), 3.0));
        grupa.add(new Student("Niewiadomski", LocalDate.of(1999, 6, 20), 3.5));
        grupa.add(new Student("Stankiewicz", LocalDate.of(1946, 5, 10), 2.5));
        grupa.add(new Student("Mickiewicz", LocalDate.of(1998, 6, 20), 5.0));

        for (Osoba i : grupa) {
            System.out.println(i);
        }
        System.out.println();

        Collections.sort(grupa);

        for (Osoba i : grupa) {
            System.out.println(i);
        }

    }
}
