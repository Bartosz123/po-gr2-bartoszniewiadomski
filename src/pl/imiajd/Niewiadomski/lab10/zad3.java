package pl.imiajd.Niewiadomski.lab10;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class zad3 {
    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<String> arrayList = new ArrayList<>();
        File file = new File(args[0]);
        Scanner scanner = new Scanner(file);

        while (scanner.hasNextLine()) {
            arrayList.add(scanner.nextLine());
        }
        scanner.close();

        Collections.sort(arrayList);
        System.out.println(arrayList);
    }

}
