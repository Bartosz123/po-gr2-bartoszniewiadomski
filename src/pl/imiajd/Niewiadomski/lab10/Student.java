package pl.imiajd.Niewiadomski.lab10;
import java.time.LocalDate;

public class Student extends Osoba implements Cloneable, Comparable<Osoba>{
    private double sredniaOcen;

    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    public String toString() {
        return this.getClass() + " [Nazwisko: " + getNazwisko() + " Data urodzenia: " + getDataUrodzenia() + " Srednia: " + sredniaOcen + "]";
    }

    public int compareTo(Osoba o) {
        if(super.compareTo(o) == 0) {
            return (int)(this.sredniaOcen - ((Student)o).sredniaOcen);
        }
        return super.compareTo(o);
    }
}
