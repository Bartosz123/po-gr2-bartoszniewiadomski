package pl.imiajd.Niewiadomski.lab10;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba {
    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList<>();

        grupa.add(new Osoba("Kowalski", LocalDate.of(1898, 12, 24)));
        grupa.add(new Osoba("Stonoga", LocalDate.of(1840, 5, 5)));
        grupa.add(new Osoba("Niewiadomski", LocalDate.of(1999, 6, 20)));
        grupa.add(new Osoba("Sienkiewicz", LocalDate.of(1856, 5, 5)));
        grupa.add(new Osoba("Mickiewicz", LocalDate.of(1548, 12, 24)));

        for (Osoba i : grupa) {
            System.out.println(i);
        }
        System.out.println();

        Collections.sort(grupa);

        for (Osoba i : grupa) {
            System.out.println(i);
        }
    }
}
