package pl.imiajd.Niewiadomski.lab7;

public class Adres {
    private String ulica;
    private int numerDomu;
    private int numerMieszkania;
    private String miasto;
    private String kodPocztowy;

    public Adres(String ulica, int numerDomu, int numerMieszkania, String miasto, String kodPocztowy) {
        this.ulica = ulica;
        this.numerDomu = numerDomu;
        this.numerMieszkania = numerMieszkania;
        this.miasto = miasto;
        this.kodPocztowy = kodPocztowy;

    }

    public Adres(String ulica, int numerDomu, String miasto, String kodPocztowy) {
        this.ulica = ulica;
        this.numerDomu = numerDomu;
        this.miasto = miasto;
        this.kodPocztowy = kodPocztowy;
    }

    public void pokaz(){
        System.out.println(kodPocztowy + " " + miasto);
        System.out.println(ulica + " " + numerDomu + " " + numerMieszkania);
    }

    public boolean przed(Adres otherObject){
        if (kodPocztowy.equals(otherObject.kodPocztowy))
            return true;
        return false;
    }
}