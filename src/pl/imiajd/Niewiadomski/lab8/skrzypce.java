package pl.imiajd.Niewiadomski.lab8;
import java.time.LocalDate;

public class skrzypce extends Instrument {

    public skrzypce(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    @Override
    void dzwiek() {
        System.out.println("dzwiek Skrzypiec");

    }
}
