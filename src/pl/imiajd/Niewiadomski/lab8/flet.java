package pl.imiajd.Niewiadomski.lab8;
import java.time.LocalDate;

public class flet extends Instrument{

    public flet(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    @Override
    void dzwiek() {
        System.out.println("dzwiek Fletu");
    }
}

