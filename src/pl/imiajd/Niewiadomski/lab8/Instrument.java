package pl.imiajd.Niewiadomski.lab8;
import java.time.LocalDate;

public abstract class Instrument {
    private String producent;
    private LocalDate rokProdukcji;

    public Instrument(String producent, LocalDate rokProdukcji) {
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public String getProducent() {
        return producent;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }

    abstract void dzwiek();

    public boolean equals(Instrument otherObject) {
        if (this == otherObject)
            return true;

        if (this.producent.equals(otherObject.producent) && this.rokProdukcji == otherObject.rokProdukcji)
            return true;
        return false;

    }

    public String toString() {
        return "producent: " + producent + ", rok Produkcji: " + rokProdukcji;
    }
}
