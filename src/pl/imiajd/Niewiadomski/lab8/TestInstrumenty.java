package pl.imiajd.Niewiadomski.lab8;
import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {
    public static void main(String[] args) {
        ArrayList<Instrument> orkiestra = new ArrayList<>();
        orkiestra.add(new fortepian("a", LocalDate.of(1999, 11, 9)));
        orkiestra.add(new skrzypce("b", LocalDate.of(1657, 1, 22)));
        orkiestra.add(new flet("c", LocalDate.of(1543, 3, 3)));
        orkiestra.add(new fortepian("d", LocalDate.of(1657, 6, 13)));
        orkiestra.add(new flet("e", LocalDate.of(1543, 4, 12)));

        for (Instrument i : orkiestra) {
            i.dzwiek();
        }

        System.out.println();

        for (Instrument i : orkiestra) {
            System.out.println(i);
        }

    }
}
