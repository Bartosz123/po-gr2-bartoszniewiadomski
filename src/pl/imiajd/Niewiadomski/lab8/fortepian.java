package pl.imiajd.Niewiadomski.lab8;
import java.time.LocalDate;

public class fortepian extends Instrument {

    public fortepian(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    @Override
    void dzwiek() {
        System.out.println("dzwiek fortepianu");
    }
}
