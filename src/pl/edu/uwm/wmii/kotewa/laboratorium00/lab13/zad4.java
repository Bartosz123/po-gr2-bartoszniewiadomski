package pl.edu.uwm.wmii.kotewa.laboratorium00.lab13;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
public class zad4 {
    public static void main(String[] args) throws FileNotFoundException{
        File plik = new File("src\\pl\\edu\\uwm\\wmii\\kotewa\\laboratorium00\\lab13\\bartek");

        func(plik);
    }

    public static void func(File plik) throws FileNotFoundException {
        Scanner wczytanyPlik = new Scanner(plik);
        Map<Integer, HashSet<String>> mapa = new HashMap<>();

        while(wczytanyPlik.hasNext()){
            String slowo = wczytanyPlik.next();
            Integer h = slowo.hashCode();
            if(mapa.containsKey(h)){
                mapa.get(h).add(slowo);
            } else {
                HashSet<String> temp = new HashSet<>();
                temp.add(slowo);
                mapa.put(h, temp);
            }

        }
        wczytanyPlik.close();

        for(Map.Entry<Integer, HashSet<String>> map : mapa.entrySet()){
            if(map.getValue().size() > 1){
                Iterator<String> iter = map.getValue().iterator();
                while(iter.hasNext()){
                    System.out.println("[hashCode: " + map.getKey() + " slowo: " + iter.next() + "]");
                }

            }
        }

    }
}
