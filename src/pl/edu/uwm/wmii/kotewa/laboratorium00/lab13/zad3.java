package pl.edu.uwm.wmii.kotewa.laboratorium00.lab13;
import java.util.*;
public class zad3 {
    private static Map<Student, String> mapa = new TreeMap<>();

    public static void main(String[] args){
        program();
    }

    private static void dodaj(Student stu, String ocena){
        mapa.put(stu, ocena);
    }

    private static void usun(Integer id){
        Iterator<Map.Entry<Student, String>> it = mapa.entrySet().iterator();
        while (it.hasNext()) {
            if(it.next().getKey().equals(id)){
                it.remove();
            }
        }
    }

    private static void wyswietl(){
        System.out.println("---------LISTA---------");
        for(Map.Entry<Student, String> o : mapa.entrySet()){
            System.out.println(o.getKey() + ": " + o.getValue() );
        }
        System.out.println("--------------------------");
    }

    private static void zmien(Integer id, String ocena){
        for(Map.Entry<Student, String> o : mapa.entrySet()){
            if(o.getKey().equals(id)){
                mapa.put(o.getKey(), ocena);
            }
        }
    }



    public static void program(){
        Scanner sc = new Scanner(System.in);

        while(true) {
            System.out.println("________________________");
            System.out.println("1. 'dodaj'.");
            System.out.println("2. 'usun'.");
            System.out.println("3. 'wyswietl'.");
            System.out.println("4. 'zmien'.");
            System.out.println("5. 'zakoncz'.");
            System.out.print("polecenie:  ");
            String polecenie = sc.nextLine();

            if(polecenie.equals("dodaj")){
                System.out.print("Podaj nazwisko do dodania: ");
                String nazwisko = sc.nextLine();
                System.out.print("Podaj imie: ");
                String imie = sc.nextLine();
                System.out.print("Podaj ocene: ");
                String ocena = sc.nextLine();
                dodaj(new Student(nazwisko, imie), ocena);
            }

            if(polecenie.equals("usun")){
                System.out.print("Podaj identyfikator do usuniecia: ");
                Integer id = sc.nextInt();
                usun(id);
            }

            if(polecenie.equals("wyswietl")){
                wyswietl();
            }

            if(polecenie.equals("zmien")){
                System.out.print("Podaj identyfikator do zmiany: ");
                Integer id = sc.nextInt();
                sc.nextLine();
                System.out.print("Podaj ocene do zmiany: ");
                String ocena = sc.nextLine();
                zmien(id, ocena);
            }

            if(polecenie.equals("zakoncz")){
                break;
            }

        }


    }
}

class Student implements Comparable<Student>{
    private String imie;
    private String nazwisko;
    private Integer id;
    private static Integer iloscStudentow = 0;

    Student(String nazwisko, String imie){
        this.nazwisko = nazwisko;
        this.imie = imie;
        iloscStudentow++;
        this.id = iloscStudentow;
    }

    public boolean equals(Integer id){
        return this.id.equals(id);
    }

    @Override
    public int compareTo(Student obiekt) {
        if(this.nazwisko.compareTo(obiekt.nazwisko) != 0){
            return this.nazwisko.compareTo(obiekt.nazwisko);
        }
        if(this.imie.compareTo(obiekt.imie) != 0){
            return this.imie.compareTo(obiekt.imie);
        }
        return this.id.compareTo(obiekt.id);
    }

    @Override
    public String toString(){
        return id + " " + imie + " " + nazwisko;
    }
}
