package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab4;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class zad3 {
    public static int func(String nazwaPliku, String wyraz)throws Exception{
        File plik = new File("src\\pl\\edu\\uwm\\wmii\\kotewa\\laboratorium00\\Lab4\\" + nazwaPliku + ".txt");
        plik.createNewFile();

        PrintWriter temp = new PrintWriter(plik);
        temp.print("Bartek ma 4 psy i 6 kotow");
        temp.close();

        Scanner scanner = new Scanner(plik);

        int ile = 0;

        while(scanner.hasNext()){
            String linia = scanner.next();
            System.out.println(linia);
            if(linia.equals(wyraz))
                ile++;
        }
        scanner.close();
        return ile;
    }

    public static void main(String[] args)throws Exception{
        int zmienna = func("tekst", "ma");
        System.out.println("ilosc wyrazow w pliku: " + zmienna);
    }
}
