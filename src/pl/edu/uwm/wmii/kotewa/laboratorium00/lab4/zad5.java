package pl.edu.uwm.wmii.kotewa.laboratorium00.lab4;
import java.math.BigDecimal;
import java.math.RoundingMode;
public class zad5 {
    public static BigDecimal func(BigDecimal k, BigDecimal p, int n){

        BigDecimal wynik = new BigDecimal(k.toString());

        for(int i = 0; i < n; i++){
            k = k.multiply(p);
            wynik = wynik.add(k);
        }

        return wynik.setScale(2, RoundingMode.FLOOR);


    }

    public static void main(String[] args){
        BigDecimal kapital = new BigDecimal("100");
        BigDecimal stopaProcentowa = new BigDecimal("4.7");
        int okresOszczedzania = 10;
        System.out.println(func(kapital, stopaProcentowa, okresOszczedzania));

    }
}
