package pl.edu.uwm.wmii.kotewa.laboratorium00.lab11;

public class Pair<T> {

    public Pair() {
        pierwszy = null;
        drugi = null;
    }

    public Pair (T first, T second) {
        this.pierwszy = first;
        this.drugi = second;
    }

    public void swap(){
        T temp = pierwszy;
        pierwszy = drugi;
        drugi = temp;
    }

    public T getFirst() {
        return pierwszy;
    }
    public T getSecond() {
        return drugi;
    }

    public void setFirst (T newValue) {
        pierwszy= newValue;
    }
    public void setSecond (T newValue) {
        drugi = newValue;
    }

    private T pierwszy;
    private T drugi;
}
