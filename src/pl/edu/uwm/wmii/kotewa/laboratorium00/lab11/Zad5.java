package pl.edu.uwm.wmii.kotewa.laboratorium00.lab11;
import java.time.LocalDate;
import java.util.Arrays;

public class Zad5 {
    public static void main(String[] args) {
        Integer[] tab = {5,-4,10,15,20,1,50,90,-90,100};

        System.out.println("tab przed sortowaniem: " + Arrays.asList(tab));

        ArrayUtil.selectionSort(tab);

        System.out.println("tab po sortowaniu: " + Arrays.asList(tab));
        System.out.println();

        LocalDate[] daty = new LocalDate[]{LocalDate.of(1300, 10, 25),
                LocalDate.of(1900, 5, 25),
                LocalDate.of(1796, 4, 11),
                LocalDate.of(1939, 3, 2),
                LocalDate.of(1999, 6, 20)};


        System.out.println("talica daty przed sortowaniem: " + Arrays.asList(daty));

        ArrayUtil.selectionSort(daty);

        System.out.println("tablica daty po sortowaniu: " + Arrays.asList(daty));

    }
}

