package pl.edu.uwm.wmii.kotewa.laboratorium00.lab11;
import java.time.LocalDate;
import java.util.Arrays;


public class Zad3 {
    public static void main(String[] args) {
        Integer[] tab = {5, 8, 16, 20 ,4, 3};

        System.out.println(Arrays.asList(tab));
        System.out.println("Czy jest uporządkowana niemalejaco: " + ArrayUtil.isSorted(tab));
        System.out.println();

        Integer[] tabSorted = {5, 8, 16, 18, 10, 15};

        System.out.println(Arrays.asList(tabSorted));
        System.out.println("Czy jest uporządkowana niemalejaco: " + ArrayUtil.isSorted(tabSorted));
        System.out.println();

        LocalDate[] daty = new LocalDate[]{LocalDate.of(1339, 2, 25),
                LocalDate.of(1999, 6, 20),
                LocalDate.of(1955, 4, 15),
                LocalDate.of(1939, 6, 20),
                LocalDate.of(1944, 5, 31)};

        System.out.println(Arrays.asList(daty));
        System.out.println("Czy jest uporządkowana niemalejaco: " + ArrayUtil.isSorted(daty));
        System.out.println();

        LocalDate[] datySorted = new LocalDate[]{LocalDate.of(1499, 1, 25),
                LocalDate.of(2090, 10, 30),
                LocalDate.of(2056, 7, 1),
                LocalDate.of(1739, 11, 20),
                LocalDate.of(1997, 5, 31)};

        System.out.println(Arrays.asList(datySorted));
        System.out.println("Czy jest uporządkowana niemalejaco: " + ArrayUtil.isSorted(datySorted));




    }
}
