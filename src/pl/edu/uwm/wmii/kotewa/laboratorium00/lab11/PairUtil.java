package pl.edu.uwm.wmii.kotewa.laboratorium00.lab11;

public class PairUtil {

    public static <T> Pair<T> swap(Pair<T> ob) {
        return new Pair<>(ob.getSecond(), ob.getFirst());
    }
}
