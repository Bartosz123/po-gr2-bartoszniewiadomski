package pl.edu.uwm.wmii.kotewa.laboratorium00.lab11;
import java.util.Arrays;

public class ArrayUtil {

    public static <T extends Comparable<T>> boolean isSorted(T[] tab) {
        for (int i = 0; i < tab.length - 1; i++) {
            if(tab[i].compareTo(tab[i + 1]) >= 0){
                return false;
            }
        }
        return true;
    }

    public static <T extends Comparable<T>> int binSearch(T[] tab, T szukana) {
        int lewa = 0;
        int prawa = tab.length;

        while (lewa <= prawa){
            int srodek = lewa + (prawa - lewa) / 2;

            if(tab[srodek].compareTo(szukana) == 0){
                return srodek;
            }
            if(tab[srodek].compareTo(szukana) < 0){
                lewa = srodek + 1;
            }
            if(tab[srodek].compareTo(szukana) > 0){
                prawa = srodek - 1;
            }
        }
        return -1;
    }

    public static <T extends Comparable<T>> void selectionSort(T[] tab) {
        for(int i = 0; i < tab.length - 1; i++) {
            int minIndex = i;

            for(int j = i + 1; j < tab.length; j++) {
                if (tab[minIndex].compareTo(tab[j]) > 0) {
                    minIndex = j;
                }
            }

            T temp = tab[i];
            tab[i] = tab[minIndex];
            tab[minIndex] = temp;
        }
    }

    public static <T extends Comparable<T>> void mergeSort(T[] tab) {
        if(tab.length <= 1) return;

        int srodek = tab.length / 2;

        T[] lewa = Arrays.copyOfRange(tab, 0, srodek);

        T[] prawa = Arrays.copyOfRange(tab, srodek, tab.length);


        mergeSort(lewa);
        mergeSort(prawa);

        int i = 0, j = 0, k = 0;

        while(i < lewa.length && j < prawa.length) {
            if(lewa[i].compareTo(prawa[j]) < 0) {
                tab[k] = lewa[i];
                i++;
            } else {
                tab[k] = prawa[j];
                j++;
            }
            k++;
        }

        while(i < lewa.length) {
            tab[k] = lewa[i];
            i++;
            k++;
        }

        while(j < prawa.length) {
            tab[k] = prawa[j];
            j++;
            k++;
        }

    }

}
