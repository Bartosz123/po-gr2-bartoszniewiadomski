package pl.edu.uwm.wmii.kotewa.laboratorium00.lab11;
import java.time.LocalDate;

public class Zad4 {
    public static void main(String[] args) {
        Integer[] tab = new Integer[]{2, 4, 5, 7, 10, 13, 15, 28, 30, 50,100,200,300,400,500,800,1000};
        System.out.println("Index 500 w tab: " + ArrayUtil.binSearch(tab, 500));


        LocalDate[] datyTab = new LocalDate[]{LocalDate.of(1299, 1, 25),
                LocalDate.of(1350, 10, 30),
                LocalDate.of(1356, 11, 15),
                LocalDate.of(1939, 12, 20),
                LocalDate.of(2020, 4, 3)};

        System.out.println("Index daty 2020-4-31 w datyTab: " + ArrayUtil.binSearch(datyTab, LocalDate.of(2020, 4, 3)));
    }
}
