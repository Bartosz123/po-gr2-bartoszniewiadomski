package pl.edu.uwm.wmii.kotewa.laboratorium00.lab11;

public class Zad2 {
    public static void main(String[] args) {
        Pair<Integer> ob1 = new Pair<>(1, 10);
        System.out.println(ob1.getFirst() + " " + ob1.getSecond());

        Pair<Integer> ob2 = PairUtil.swap(ob1);
        System.out.println(ob2.getFirst() + " " + ob2.getSecond());
    }
}
