package pl.edu.uwm.wmii.kotewa.laboratorium00.lab2;

import java.util.Scanner;

public class zad2a5 {
    public static void funkcja(int n){
        Scanner liczby = new Scanner(System.in);
        int tab[]=new int[n];
        for(int i=0;i<n;i++){
            System.out.println("podaj liczbe: ");
            tab[i]=liczby.nextInt();
        }
        int para=0;
        for(int i=0;i<n-1;i++){
            if(tab[i]>0 && tab[i+1]>0){
                para++;
            }
        }
        System.out.printf("par jest: %d ",para);
    }
    public static void main(String[] args){
        Scanner liczby =new Scanner(System.in);
        System.out.println("Podaj ilosc liczb: ");
        int n=liczby.nextInt();
        funkcja(n);
    }
}
