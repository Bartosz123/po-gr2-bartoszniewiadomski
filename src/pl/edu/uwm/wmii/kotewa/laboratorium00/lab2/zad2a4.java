package pl.edu.uwm.wmii.kotewa.laboratorium00.lab2;
import java.util.Scanner;
import static java.lang.Math.*;
public class zad2a4 {
    public static void funkcja(int n){
        Scanner liczby=new Scanner(System.in);
        int tablica[]=new int[n];
        for (int i=0;i<n;i++){
            System.out.println("podaj liczbe: ");
            tablica[i]=liczby.nextInt();
        }
        int najmniejsza=0;
        int najwieksza=0;
        for(int i=0;i<n;i++){
            if(tablica[i]<najmniejsza){
                najmniejsza = tablica[i];
            }
            if(tablica[i]>najwieksza){
                najwieksza = tablica[i];
            }
        }
        System.out.printf("najwieksza liczba jest: %d ",najwieksza);
        System.out.printf("najmniejsza liczba jest :%d ",najmniejsza);
    }
    public static void main(String[] args){
        Scanner liczby=new Scanner(System.in);
        System.out.println("podaj ilosc liczb: ");
        int n=liczby.nextInt();
        funkcja(n);

    }
}
