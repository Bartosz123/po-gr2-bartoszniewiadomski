package pl.edu.uwm.wmii.kotewa.laboratorium00.lab2;
import java.util.Scanner;
import static java.lang.Math.*;
public class zad1a1 {
    public static float suma(int n) {
        Scanner in = new Scanner(System.in);
        float a = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            float b = in.nextFloat();
            a += b;
        }
        return a;
    }
    public static float iloczyn(int n) {
        Scanner in = new Scanner(System.in);
        float a = 1;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            float b = in.nextFloat();
            a *= b;
        }
        return a;
    }
    public static float bezwsuma(int n) {
        Scanner in = new Scanner(System.in);
        float a = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            float b = in.nextFloat();
            if (b < 0){
                b *= (-1);
            }
            a += b;
        }
        return a;
    }
    public static double pierwbezwsuma(int n) {
        Scanner in = new Scanner(System.in);
        double a = 0;
        double c = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            double b = in.nextDouble();
            if (b < 0){
                b *= (-1);
            }
            c = Math.sqrt(b);
            a += c;
        }
        return c;
    }
    public static float bezwiloczyn(int n) {
        Scanner in = new Scanner(System.in);
        float a = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            float b = in.nextFloat();
            if (b < 0){
                b *= (-1);
            }
            a *= b;
        }
        return a;
    }
    public static float kwsuma(int n) {
        Scanner in = new Scanner(System.in);
        float a = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            float b = in.nextFloat();
            b *=b;
            a += b;
        }
        return a;
    }
    public static void sumaoiloczyn(int n) {
        Scanner in = new Scanner(System.in);
        float s = 0;
        float il = 1;
        float b = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            b = in.nextFloat();
            s += b;
            il *= b;
        }
        System.out.println("Suma = " + s);
        System.out.println("Iloczyn = " + il);
    }
    public static double ciag(int n){
        Scanner in = new Scanner(System.in);
        double a = 0;
        double b = 0;
        for (int i = 0; i < n; i++){
            System.out.println("Podaj liczbe: ");
            b = in.nextDouble();
            b = Math.pow(-1,i+1)*b;
            a +=b;
        }
        return a;
    }
    public static double ciag2(int n){
        Scanner in = new Scanner(System.in);
        double a = 0;
        double b = 0;
        int silnia = 1;
        for(int i = 0; i < n; i++) {
            silnia *= (i+1);
            System.out.println("Podaj liczbe: ");
            b = in.nextDouble();
            b = (pow(-1,i+1)*b)/silnia;
            a += b;
        }
        return a;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj ilosc liczb: ");
        int n = in.nextInt();
        sumaoiloczyn(n);
    }

}
