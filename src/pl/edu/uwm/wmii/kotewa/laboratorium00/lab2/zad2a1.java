package pl.edu.uwm.wmii.kotewa.laboratorium00.lab2;
import java.util.Scanner;
import static java.lang.Math.*;
public class zad2a1 {
    public static int npar(int n){
        int liczba = 0;
        for(int i = 0; i < n; i++){
            if(i % 2 !=0){
                liczba++;
            }
        }
        return liczba;
    }

    public static int podz3npodz5(int n){
        int x = 0;
        for(int i = 0; i< n; i++){
            if(i%3==0 && i%5 !=0){
                x++;
            }
        }
        return x;
    }

    public static int kwadratparz(int n){
        int l=0;
        for(double i = 0; i<n; i++){
            double a = Math.pow(i,(1/2));
            if(a%2==0){
                l++;
            }
        }
        return l;
    }

    public static int warunek1(int n){
        int y=0;
        int k;
        for(int i = 1; i<n; i++){
            k = 1;
            if(k < ((i-1)+(i+1))){
                y++;
            }
        }
        return y;
    }



    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbę: ");
        int n;
        n = in.nextInt();
        int l = npar(n);
        System.out.println(l);

    }
}
