package pl.edu.uwm.wmii.kotewa.laboratorium00.lab7;
import pl.imiajd.Niewiadomski.lab7.NazwanyPunkt;
import pl.imiajd.Niewiadomski.lab7.Punkt;

public class zad1
{
    public static void main(String[] args)
    {
        NazwanyPunkt a = new NazwanyPunkt(3, 5, "port");
        a.show();

        Punkt b = new Punkt(3, 5);
        b.show();

        Punkt c = new NazwanyPunkt(3, 6, "tawerna");
        c.show();

        a = (NazwanyPunkt) c;
        a.show();
    }
}
