package pl.edu.uwm.wmii.kotewa.laboratorium00.lab7;
import pl.imiajd.Niewiadomski.lab7.Nauczyciel;
import pl.imiajd.Niewiadomski.lab7.Osoba;
import pl.imiajd.Niewiadomski.lab7.Student;

public class zad4 {
    public static void main(String[] args) {
        Osoba osoba = new Osoba("Kowlalski", 1999);
        Student student = new Student("Nowak", 1990, "Informatyka");
        Nauczyciel nauczyciel = new Nauczyciel("Niewiadomski", 1995, 5000);

        System.out.println(osoba);
        System.out.println(student);
        System.out.println(nauczyciel);

        System.out.println(osoba.getNazwisko());
        System.out.println(student.getNazwisko());
        System.out.println(nauczyciel.getNazwisko());

        System.out.println(student.getKierunek());
        System.out.println(nauczyciel.getPensja());
    }

}
