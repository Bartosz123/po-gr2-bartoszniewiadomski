package pl.edu.uwm.wmii.kotewa.laboratorium00.lab5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static java.lang.System.*;

public class zad3 {
    public static void main(String [] args){
        ArrayList<Integer> lista = new ArrayList<Integer>(Arrays.asList(1,4,9,16));
        ArrayList<Integer> lista2 = new ArrayList<Integer>(Arrays.asList(9,7,4,9,11));
        out.println(lista);
        out.println(lista2);
        out.println(mergeSorted(lista,lista2));
    }
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> lista3 = new ArrayList<Integer>();
        Collections.sort(a);
        Collections.sort(b);
        lista3.addAll(0,a);
        lista3.addAll(a.size(),b);
        Collections.sort(lista3);
        return lista3;
    }
}
