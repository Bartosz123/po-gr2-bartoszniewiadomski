package pl.edu.uwm.wmii.kotewa.laboratorium00.lab5;

import java.util.ArrayList;
import java.util.Arrays;

import static java.lang.System.out;

public class zad5 {
    public static void main(String [] args){
        ArrayList<Integer> lista = new ArrayList<Integer>(Arrays.asList(1,4,9,16));
        out.println(lista);
        reverse(lista);
    }
    public static void reverse(ArrayList<Integer> a){
        int temp =0;
        for(int i=a.size()-1;i>=a.size()/2;i--){
            temp = a.get(i);
            a.set(i,a.get(a.size()-1-i));
            a.set(a.size()-1-i,temp);
        }
        System.out.println(a);
    }
}
