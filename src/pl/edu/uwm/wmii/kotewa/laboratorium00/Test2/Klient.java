package pl.edu.uwm.wmii.kotewa.laboratorium00.Test2;

import java.time.LocalDate;

public class Klient implements Cloneable,Comparable<Klient> {
    private String nazwa;
    private int id;
    private LocalDate dataZakupy;
    private double rachunek;

    public Klient(String nazwa, int id,LocalDate dataZakupy,double rachunek) {
        this.nazwa = nazwa;
        this.id = id;
        this.dataZakupy = dataZakupy;
        this.rachunek = rachunek;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getId() {
        return id;
    }

    public LocalDate getDataZakupy() {
        return dataZakupy;
    }

    public double getrachunek() {
        return rachunek;
    }


    @Override
    public int compareTo(Klient o) {
        if (this.dataZakupy.compareTo(o.dataZakupy) == 0) {
            if (this.nazwa.equals(o.nazwa)) {
                return 1;
            }
            if (this.nazwa.equals(o.nazwa)) {
                return -1;
            }
            if (this.nazwa == o.nazwa) {
                if (this.rachunek > o.rachunek) {
                    return 1;
                }
                if (this.rachunek < o.rachunek) {
                    return -1;
                }
                if (this.rachunek == o.rachunek) {
                    return 0;
                }
            }
        }return this.dataZakupy.compareTo(o.dataZakupy);
    }
    @Override
    public String toString() {
        return "Klient:" + "\n  nazwa: " + nazwa + "\n  id: "  + id  + "\n  DataZakupów: " +dataZakupy +"\n  rachunek: "+rachunek+"\n";
    }

}

