package pl.edu.uwm.wmii.kotewa.laboratorium00.Test2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        ArrayList<Klient> grupa = new ArrayList<>();
        grupa.add(new Klient("Kowalski",22, LocalDate.of(2020,12,20),100));
        grupa.add(new Klient("Brzeczyszczykiewicz",22,LocalDate.of(2020,12,20),350));
        grupa.add(new Klient("Sienkiewicz",32,LocalDate.of(2020,11,12),200));
        grupa.add(new Klient("Parandowski",22,LocalDate.of(2020,12,12),200));
        grupa.add(new Klient("Niewiadomski",2,LocalDate.of(2019,12,12),150));
        grupa.add(new Klient("Niewiadomski",22,LocalDate.of(2020,11,12),175));
        Obsługaklienta.setProcentRabatu();
        System.out.println("Lista Klientow");
        for(Klient i : grupa){
            System.out.println(i);
        }
        Collections.sort(grupa);
        System.out.println("Lista Klientow po sortowaniu\n");
        for(Klient i : grupa){
            System.out.println(i);
        }
        System.out.println("\n\n");
        System.out.print("Wynik: ");
        System.out.println(Obsługaklienta.discountAmount(grupa));
    }
}
