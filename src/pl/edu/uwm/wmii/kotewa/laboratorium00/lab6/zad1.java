package pl.edu.uwm.wmii.kotewa.laboratorium00.lab6;

public class zad1{
    public static void main(String [] args){
        RachunekBankowy saver1= new RachunekBankowy();
        saver1.name = "saver1";
        saver1.saldo=2000;

        RachunekBankowy saver2 = new RachunekBankowy();
        saver2.name="saver2";
        saver2.saldo=3000;

        RachunekBankowy.setRocznaStopaProcentowa(4);
        System.out.println(saver1.obliczMiesieczneOdsetki());
        System.out.println(saver2.obliczMiesieczneOdsetki());

        RachunekBankowy.setRocznaStopaProcentowa(5);
        System.out.println(saver1.obliczMiesieczneOdsetki());
        System.out.println(saver2.obliczMiesieczneOdsetki());

    }
}

