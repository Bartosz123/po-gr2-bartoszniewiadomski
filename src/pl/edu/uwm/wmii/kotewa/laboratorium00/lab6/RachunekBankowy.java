package pl.edu.uwm.wmii.kotewa.laboratorium00.lab6;

public class RachunekBankowy {
    String name;
    double saldo;
    static double rocznaStopaProcentowa=1.2;

    double obliczMiesieczneOdsetki(){
        this.saldo=saldo;
        double wynik =0;
        wynik = (this.saldo * rocznaStopaProcentowa) / 12;
        saldo=saldo+wynik;
        return saldo;
    }
    static void setRocznaStopaProcentowa(double liczba){
        rocznaStopaProcentowa=liczba;
    }
}
