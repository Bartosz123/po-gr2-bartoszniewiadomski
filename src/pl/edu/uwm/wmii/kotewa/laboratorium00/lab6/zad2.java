package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab6;



public class zad2 {
    public static void main(String[] args){
        IntegerSet tab1 = new IntegerSet();
        IntegerSet tab2 = new IntegerSet();
        IntegerSet tab3 = new IntegerSet();

        for(int i = 0; i < 100; i = i + 3){
            tab1.insertElement(i);
            tab3.insertElement(i);
        }

        for (int i = 0; i < 100; i += 5){
            tab2.insertElement(i);
        }

        System.out.println("tab1: " + tab1);
        System.out.println("tab2: " + tab2);
        System.out.println("tab3: " + tab3);
        System.out.println("czy tab1 jest rowny tab2: " + tab1.equals(tab2.set));
        System.out.println("czy tab1 jest rowny tab3: " + tab1.equals(tab3.set));
        tab1.insertElement(50);
        System.out.println("czy tab1 z dodatkowym 50 jest rowny tab3: " + tab1.equals(tab3.set));
        tab1.deleteElement(50);
        System.out.println("czy tab1 bez 50 jest rowny tab3: " + tab1.equals(tab3.set));
        System.out.println("Czy tab1 jest rowny iloczynowi tab1 i tab3: "+ tab1.equals(IntegerSet.intersection(tab1.set, tab3.set)));
        System.out.println("Czy tab1 jest rowny sumie tab1 i tab2: "+ tab1.equals(IntegerSet.union(tab1.set, tab2.set)));

    }
}
