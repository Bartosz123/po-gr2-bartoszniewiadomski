package pl.edu.uwm.wmii.kotewa.laboratorium00.lab12;

import java.util.Scanner;
import java.util.Stack;

public class zad6 {
    public static void main(String args[]){
        Stack<Integer> stack=new Stack<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("podaj liczbe: ");
        Integer liczba=scanner.nextInt();
        while(liczba>0){
            stack.push(liczba%10);
            liczba/=10;
        }
        while(!stack.isEmpty()){
            System.out.print(stack.pop()+" ");
        }
    }
}
