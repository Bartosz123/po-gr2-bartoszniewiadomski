package pl.edu.uwm.wmii.kotewa.laboratorium00.lab12;

import java.util.Stack;

public class zad5 {
    public static void main(String args[]){
        Stack<String>stos = new Stack<>();
        String zdanie="Ala ma kota. Jej kot lubi myszy.";
        String[] podzielenie=zdanie.split(" ");
        String koniec="";

        for(int i=0;i<podzielenie.length;i++){
            if(!podzielenie[i].endsWith(".")){
                podzielenie[i]=Character.toLowerCase(podzielenie[i].charAt(0))+podzielenie[i].substring(1);
                stos.push(podzielenie[i]);
            }else{
                podzielenie[i]=podzielenie[i].substring(0,podzielenie[i].length()-1);
                stos.push(podzielenie[i]);
                String pomoc = "";
                while(!stos.isEmpty()){
                    pomoc += stos.pop()+" ";
                }
                pomoc = Character.toUpperCase(pomoc.charAt(0))+pomoc.substring(1);
                pomoc=pomoc.substring(0,pomoc.length()-1);
                koniec+= pomoc +". ";
            }

        }
        System.out.println(koniec);
    }
}
