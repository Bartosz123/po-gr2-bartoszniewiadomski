package pl.edu.uwm.wmii.kotewa.laboratorium00.lab12;

import java.util.LinkedList;

public class zad3 {
    public static void main (String args[]){
        LinkedList<String> pracownicy=new LinkedList<String>();
        pracownicy.add("Niewiadomski");
        pracownicy.add("Kowalski");
        pracownicy.add("Nowak");
        pracownicy.add("Kiepski");
        pracownicy.add("Pazdzioch");
        pracownicy.add("Sienkiewicz");
        pracownicy.add("Słowacki");
        pracownicy.add("Pawlak");
        System.out.println(pracownicy);
        odwroc(pracownicy);
        System.out.println(pracownicy);
    }
    public static void odwroc(LinkedList<String> lista ){
        LinkedList<String> pomoc= new LinkedList<>(lista);
        for(int i = lista.size() - 1, j = 0; i >= 0; i--, j++) {
            lista.set(j, pomoc.get(i));
        }

    }
}
