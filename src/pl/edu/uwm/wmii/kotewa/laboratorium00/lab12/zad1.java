package pl.edu.uwm.wmii.kotewa.laboratorium00.lab12;

import java.util.Iterator;
import java.util.LinkedList;

public class zad1 {
    public static void main(String args[]){
        LinkedList<String> pracownicy=new LinkedList<String>();
        pracownicy.add("Niewiadomski");
        pracownicy.add("Kowalski");
        pracownicy.add("Nowak");
        pracownicy.add("Kiepski");
        pracownicy.add("Pazdzioch");
        pracownicy.add("Sienkiewicz");
        pracownicy.add("Słowacki");
        pracownicy.add("Pawlak");
        System.out.println(pracownicy);
        redukuj(pracownicy,2);
        System.out.println(pracownicy);
    }
    public static void redukuj(LinkedList<String> pracownicy,int n){
        Iterator<String> pomoc= pracownicy.iterator();
        int i=0;
        while(pomoc.hasNext()){
            pomoc.next();
            if(i%n == n-1){
                pomoc.remove();
            }
            i++;
        }
    }
}
