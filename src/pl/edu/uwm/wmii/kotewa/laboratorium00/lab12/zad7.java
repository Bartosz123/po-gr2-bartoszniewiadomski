package pl.edu.uwm.wmii.kotewa.laboratorium00.lab12;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Stack;

public class zad7 {
    public static void main(String args[]){
        Stack<Integer> stack=new Stack<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("podaj liczbe: ");
        Integer liczba=scanner.nextInt();

        funkcja(liczba);
    }

    public static void funkcja(Integer n) {
        HashSet<Integer> pierwsze = new HashSet<>();
        for (int i = 2; i < n; i++) {
            pierwsze.add(i);
        }
        for(int i=2;i<Math.sqrt(pierwsze.size());i++){
            for(int j=i;j<n;j+=i){
                if(j!=i){
                    pierwsze.remove(j);
                }
            }
        }
        for(Integer i : pierwsze){
            System.out.println(i + " ");
        }
    }
}
