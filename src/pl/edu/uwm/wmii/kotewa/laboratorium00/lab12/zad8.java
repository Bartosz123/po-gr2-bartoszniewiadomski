package pl.edu.uwm.wmii.kotewa.laboratorium00.lab12;

import java.util.LinkedList;

public class zad8 {
    public static void main(String args[]){
        LinkedList<String> pracownicy=new LinkedList<String>();
        pracownicy.add("Niewiadomski");
        pracownicy.add("Kowalski");
        pracownicy.add("Nowak");
        pracownicy.add("Kiepski");
        pracownicy.add("Pazdzioch");
        pracownicy.add("Sienkiewicz");
        pracownicy.add("Słowacki");
        pracownicy.add("Pawlak");
        print(pracownicy);
    }
    public static <E extends Iterable<?>> void print(E arg){
        for(Object o : arg){
            System.out.print(o + ", ");
        }
    }
}
