package pl.edu.uwm.wmii.kotewa.laboratorium00.lab3;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;
public class zad3 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("podaj liczbe m: ");
        int m= scanner.nextInt();
        while(m<1 || m>10){
            System.out.println("podaj liczbe m: ");
            m=scanner.nextInt();
        }
        System.out.println("podaj liczbe n: ");
        int n=scanner.nextInt();
        while(n<1 || n>10){
            System.out.println("podaj liczbe n: ");
            n=scanner.nextInt();
        }
        System.out.println("podaj liczbe k:");
        int k=scanner.nextInt();
        while( k < 1 || k>10){
            System.out.println("podaj liczbe k:");
            k=scanner.nextInt();
        }
        Random random=new Random();
        int[][] tab1=new int[m][n];
        int[][] tab2=new int[n][k];
        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                tab1[i][j]=random.nextInt(10);
            }
        }
        for(int i=0;i<n;i++){
            for (int j=0;j<k;j++){
                tab2[i][j]=random.nextInt(10);
            }
        }
        for(int i=0; i < m; i++){
            System.out.print("[");
            for(int j = 0; j < n; j++){
                System.out.print(tab1[i][j] + ",");
            }
            System.out.println("]");
        }
        System.out.println();

        for(int i=0; i < n; i++){
            System.out.print("[");
            for(int j = 0; j < k; j++){
                System.out.print(tab2[i][j] + ",");
            }
            System.out.println("]");
        }
        System.out.println();
        int[][] tab3=new int[n][k];
        if(m == k){
            for(int wie=0; wie < m; wie++){
                for(int kol=0; kol < k; kol++){
                    tab3[wie][kol] += tab1[wie][kol] * tab2[kol][wie];
                }
            }
            for(int i=0; i < m; i++){
                System.out.print("[");
                for(int j = 0; j < k; j++){
                    System.out.print(tab3[i][j] + ",");
                }
                System.out.println("]");
            }
        }else{
            System.out.println("Mnozenie nie jest mozliwe, poniewaz liczba kolumn 'a' musi sie rownac liczbie wierszy 'b'");
        }
    }
}
